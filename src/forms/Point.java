package forms;

/**
 * A 2 dimensional point.
 *
 * @version sept. 2016
 * @author Stéphane Lopes
 */
public class Point {
	private double xxx;
	private double yyy;

	/**
	 * Initialize from 2 coordinates.
	 * 
	 * @param x
	 *            x coordinate
	 * @param y
	 *            y coordinate
	 */
	public Point(double x, double y) {
		this.xxx = x;
		this.yyy = y;
	}

	/**
	 * Get X coordinate.
	 * 
	 * @return X coordinate
	 */
	public double getX() {
		return xxx;
	}

	/**
	 * Get Y coordinate.
	 *
	 * @return Y coordinate
	 */
	public double getY() {
		return yyy;
	}

	/**
	 * Move the point.
	 * 
	 * @param dx
	 *            move in x
	 * @param dy
	 *            move in y
	 */
	public void translate(double dx, double dy) {
		xxx += dx;
		yyy += dy;
	}

	/**
	 * String description of the point.
	 * 
	 * @return the String description
	 */
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append('(');
		str.append(xxx);
		str.append(", ");
		str.append(yyy);
		str.append(')');
		return str.toString();
	}
}
